# PAR - Practical Exercise 1
## Cleaner Robotic Task

### Files
In the code folder we can found:
- Two domains
- Four problems for each domain

### Domains
In the code folder we can found both domains. Domain 1 with more generic actions and Domain 2 with more simplistic ones.

### Problem generators
To use the problem generators you should have installed python3.

It's also important to install the `numpy` library.

The command to execute them is:

```
python3 problemGeneratorDomain[1,2].py
```

After executing this command a file should be created with the generated problem. Its name will be `cleanerProblem_Domain[1,2]_TIME.pddl`, where `TIME` is the timestamp of creation.

### Problem execution
In order to execute the problems the following steps must be followed:
- Open the code folder with VSCode
- Choose and open an specific problem
- Execute the problem by pressing Alt+P and selecting the default options
- Observe the planner solution