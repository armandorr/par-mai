(define (domain cleaner-robot-domain2)
  (:requirements :strips :adl)
  (:types
    office box
  )
  (:predicates
    (robot-location ?o -office)
    (box-location ?b -box ?o -office)
    (dirty ?o -office)
    (clean ?o -office)
    (empty ?o -office)
    (up ?o1 -office ?o2 -office) ;o1 above o2
    (down ?o1 -office ?o2 -office) ;o1 below o2
    (right ?o1 -office ?o2 -office) ;o1 right o2
    (left ?o1 -office ?o2 -office) ;o1 left o2
  )

  (:action clean-office
    :parameters (?o -office)
    :precondition (and (dirty ?o) (robot-location ?o) (empty ?o))
    :effect (and (not (dirty ?o)) (clean ?o))
  )

  (:action move-up ;Move up from o1 to o2
    :parameters (?o1 -office ?o2 -office)
    :precondition (and (up ?o2 ?o1) (robot-location ?o1))
    :effect (and (not (robot-location ?o1)) (robot-location ?o2))
  )

  (:action move-down ;Move down from o1 to o2
    :parameters (?o1 -office ?o2 -office)
    :precondition (and (down ?o2 ?o1) (robot-location ?o1))
    :effect (and (not (robot-location ?o1)) (robot-location ?o2))
  )

  (:action move-right ;Move right from o1 to o2
    :parameters (?o1 -office ?o2 -office)
    :precondition (and (right ?o2 ?o1) (robot-location ?o1))
    :effect (and (not (robot-location ?o1)) (robot-location ?o2))
  )

  (:action move-left ;Move left from o1 to o2
    :parameters (?o1 -office ?o2 -office)
    :precondition (and (left ?o2 ?o1) (robot-location ?o1))
    :effect (and (not (robot-location ?o1)) (robot-location ?o2))
  )

  (:action push-up ;Push up box b from o1 to o2
    :parameters (?b -box ?o1 -office ?o2 -office)
    :precondition (and (up ?o2 ?o1) (robot-location ?o1) (box-location ?b ?o1) (empty ?o2))
    :effect (and
      (not (box-location ?b ?o1)) (box-location ?b ?o2)
      (not (empty ?o2)) (empty ?o1)
    )
  )

  (:action push-down ;Push down box b from o1 to o2
    :parameters (?b -box ?o1 -office ?o2 -office)
    :precondition (and (down ?o2 ?o1) (box-location ?b ?o1) (robot-location ?o1) (empty ?o2))
    :effect (and
      (not (box-location ?b ?o1)) (box-location ?b ?o2)
      (not (empty ?o2)) (empty ?o1)
    )
  )

  (:action push-right ;Push right box b from o1 to o2
    :parameters (?b -box ?o1 -office ?o2 -office)
    :precondition (and (right ?o2 ?o1) (box-location ?b ?o1)
      (robot-location ?o1) (empty ?o2))
    :effect (and
      (not (box-location ?b ?o1)) (box-location ?b ?o2)
      (not (empty ?o2)) (empty ?o1)
    )
  )

  (:action push-left ;Push left box b from o1 to o2
    :parameters (?b -box ?o1 -office ?o2 -office)
    :precondition (and (left ?o2 ?o1) (box-location ?b ?o1)
      (robot-location ?o1) (empty ?o2))
    :effect (and
      (not (box-location ?b ?o1)) (box-location ?b ?o2)
      (not (empty ?o2)) (empty ?o1)
    )
  )
)