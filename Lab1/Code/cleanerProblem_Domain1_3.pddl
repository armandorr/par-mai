(define (problem cleaner-robot-domain1-problem3)
	(:domain cleaner-robot-domain1)
	(:objects
		o1 o2 o3 o4 o5 o6 o7 o8 o9 -office
		b1 b2 b3 b4 b5 -box
	)
	(:init
		(adjacent o1 o2)
		(adjacent o1 o4)
		(adjacent o2 o1)
		(adjacent o2 o3)
		(adjacent o2 o5)
		(adjacent o3 o2)
		(adjacent o3 o6)
		(adjacent o4 o1)
		(adjacent o4 o5)
		(adjacent o4 o7)
		(adjacent o5 o2)
		(adjacent o5 o4)
		(adjacent o5 o6)
		(adjacent o5 o8)
		(adjacent o6 o3)
		(adjacent o6 o5)
		(adjacent o6 o9)
		(adjacent o7 o4)
		(adjacent o7 o8)
		(adjacent o8 o5)
		(adjacent o8 o7)
		(adjacent o8 o9)
		(adjacent o9 o6)
		(adjacent o9 o8)
		(robot-location o7)
		(box-location b1 o6)
		(box-location b2 o7)
		(box-location b3 o9)
		(box-location b4 o5)
		(box-location b5 o2)
		(empty o1)
		(empty o3)
		(empty o4)
		(empty o8)
		(dirty o1)
		(dirty o2)
		(dirty o3)
		(clean o4)
		(clean o5)
		(dirty o6)
		(dirty o7)
		(clean o8)
		(clean o9)
	)
	(:goal
		(and
			(clean o1)
			(clean o2)
			(clean o3)
			(clean o4)
			(clean o5)
			(clean o6)
			(clean o7)
			(clean o8)
			(clean o9)
			(box-location b1 o2)
			(box-location b2 o6)
			(box-location b3 o5)
			(box-location b4 o9)
			(box-location b5 o4)
			(robot-location o7)
		)
	)
)