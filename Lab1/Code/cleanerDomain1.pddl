(define (domain cleaner-robot-domain1)
  (:requirements :strips :adl)
  (:types
    office box
  )
  (:predicates
    (robot-location ?o -office)
    (box-location ?b -box ?o -office)
    (dirty ?o -office)
    (clean ?o -office)
    (empty ?o -office)
    (adjacent ?o1 -office ?o2 -office)
  )

  (:action clean-office
    :parameters (?o -office)
    :precondition (and (robot-location ?o) (empty ?o) (dirty ?o))
    :effect (and (not (dirty ?o)) (clean ?o))
  )

  (:action move ;Move from o1 to o2
    :parameters (?o1 -office ?o2 -office)
    :precondition (and (robot-location ?o1) (adjacent ?o1 ?o2))
    :effect (and (not (robot-location ?o1)) (robot-location ?o2))
  )

  (:action push ;Push box b from o1 to o2
    :parameters (?b -box ?o1 -office ?o2 -office)
    :precondition (and (adjacent ?o1 ?o2) (box-location ?b ?o1) 
      (robot-location ?o1) (empty ?o2))
    :effect (and
      (not (box-location ?b ?o1)) (box-location ?b ?o2)
      (not (empty ?o2)) (empty ?o1)
    )
  )
)