import random
import numpy as np
import time

def level(n):
    return "\t"*n

def endl():
    return "\n"

OUT_FILE = "cleanerProblem_Domain1_{time}.pddl".format(time=int(time.time()))

N_OFFICES = 9
N_BOXES   = random.randint(0, 8)

OFFICES  = ["o{n}".format(n=i+1) for i in range(N_OFFICES)]
BOXES    = ["b{n}".format(n=i+1) for i in range(N_BOXES)]

ROBOT_POS = [random.randint(0, 8), random.randint(0, 8)]
BOXES_POS = [random.sample(range(1, 9), N_BOXES), random.sample(range(1, 9), N_BOXES)]

CLEAN_POS = [random.randint(0, 1) for _ in range(N_OFFICES)]

output = ""
# --------------------------------------------------------------------------------------- 
output += level(0) + "(define (problem cleanerProblem)" + endl()
# ---------------------------------------------------------------------------------------
output += level(1) + "(:domain cleaner-robot-domain1)" + endl()
# ---------------------------------------------------------------------------------------
output += level(1) + "(:objects" + endl()
output += level(2) + " ".join(OFFICES) + " -office" + endl()
if N_BOXES > 0: output += level(2) + " ".join(BOXES) + " -box" + endl()
output += level(1) + ")" + endl()
# ---------------------------------------------------------------------------------------
output += level(1) + "(:init" + endl()

OFFICES_MAT = np.array(OFFICES).reshape((3,3))
for i1, row1 in enumerate(OFFICES_MAT):
    for j1, val1 in enumerate(row1):
        for i2, row2 in enumerate(OFFICES_MAT):
            for j2, val2 in enumerate(row2):
                if (abs(j1-j2) + abs(i1-i2) == 1):
                    output += level(2) + "(adjacent {office1} {office2})".format(office1=val1, office2=val2) + endl()

output += level(2) + "(robot-location {position})".format(position=OFFICES[ROBOT_POS[0]]) + endl()

positions = np.zeros(N_OFFICES)
for box, position in zip(BOXES,BOXES_POS[0]):
    output += level(2) + "(box-location {box} {position})".format(box=box,position=OFFICES[position]) + endl()
    positions[position] += 1

for idx,position in enumerate(positions):
    if position == 0:
        output += level(2) + "(empty {position})".format(position=OFFICES[idx]) + endl()
    
for idx,position in enumerate(CLEAN_POS):
    if position:
        output += level(2) + "(clean {position})".format(position=OFFICES[idx]) + endl()
    else:
        output += level(2) + "(dirty {position})".format(position=OFFICES[idx]) + endl()


output += level(1) + ")" + endl()

# ---------------------------------------------------------------------------------------
output += level(1) + "(:goal" + endl()
output += level(2) + "(and" + endl()

for office in OFFICES:
    output += level(3) + "(clean {office})".format(office=office) + endl()

for box, position in zip(BOXES,BOXES_POS[1]):
    output += level(3) + "(box-location {box} {position})".format(box=box,position=OFFICES[position]) + endl()

output += level(3) + "(robot-location {position})".format(position=OFFICES[ROBOT_POS[0]]) + endl()

output += level(2) + ")" + endl() 
output += level(1) + ")" + endl()
output += level(0) + ")"

# ---------------------------------------------------------------------------------------
text_file = open(OUT_FILE, "w")
text_file.write(output)
text_file.close()
