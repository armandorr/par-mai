(define (domain waiter-robot-domain1)
  (:requirements :strips :adl)
  (:types
    location customer plate -object
    buffet-area service-area -location
  )
  (:predicates
    ; Locations
    (adjacent ?l1 -location ?l2 -location)

    ; Robot
    (robot-location ?l -location)
    (robot-empty)
    (carrying ?p -plate)

    ; Plates
    (plate-location ?p -plate ?l -location)
    (empty ?p -plate)

    ; customers
    (customer-at ?c -customer ?l -service-area)
    (wants ?c -customer ?p -plate)
    (served-customer ?c -customer)
  )

  ; Pick up plate p from location l that has to be a buffet area
  (:action pick-up
    :parameters (?p -plate ?l -buffet-area)
    :precondition (and
      (robot-location ?l) (plate-location ?p ?l)
      (robot-empty))
    :effect (and
      (carrying ?p) (not (robot-empty)) (not (plate-location ?p ?l))
    )
  )

  ; Fill plate with food at a location l that has to be a buffet area
  (:action fill-plate
    :parameters (?p -plate ?l -buffet-area)
    :precondition (and
      (robot-location ?l)
      (carrying ?p) (empty ?p)
    )
    :effect (and (not (empty ?p)))
  )

  ; Serve plate p to a customer c in a location l that has to be a sevice area
  (:action serve-plate
    :parameters (?p -plate ?l -service-area ?c -customer)
    :precondition (and
      (not (served-customer ?c)) (robot-location ?l)
      (carrying ?p) (not (empty ?p))
      (wants ?c ?p) (customer-at ?c ?l)
    )
    :effect (and (not (carrying ?p)) (robot-empty) (served-customer ?c) (plate-location ?p ?l))
  )

  ; Move from location l1 to location l2
  (:action move
    :parameters (?l1 -location ?l2 -location)
    :precondition (and (robot-location ?l1) (adjacent ?l1 ?l2))
    :effect (and (not (robot-location ?l1)) (robot-location ?l2))
  )
)