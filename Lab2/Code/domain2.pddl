(define (domain waiter-robot-domain2)
  (:requirements :strips :adl)
  (:types
    location customer plate table -object
    buffet-area service-area -location
  )
  (:predicates
    ; Locations
    (adjacent ?l1 -location ?l2 -location)

    ; Table
    (table-location ?t -table ?l -service-area)
    (served-table ?t -table)

    ; Robot
    (robot-location ?l -location)
    (carrying ?p -plate)
    (robot-empty)

    ; Plates
    (plate-location ?p -plate ?l -location)
    (served-plate ?p -plate ?c -customer)
    (empty ?p -plate)

    ; Customers
    (customer-at ?c -customer ?t -table)
    (wants ?c -customer ?p -plate)
    (served-customer ?c -customer)
  )

  ; Pick up plate p from location l that has to be a buffet area
  (:action pick-up
    :parameters (?p -plate ?l -buffet-area)
    :precondition (and (robot-location ?l) (plate-location ?p ?l) (robot-empty))
    :effect (and (carrying ?p) (not (robot-empty)) (not (plate-location ?p ?l)))
  )

  ; Fill plate with food at a location l that has to be a buffet area
  (:action fill-plate
    :parameters (?p -plate ?l -buffet-area)
    :precondition (and (carrying ?p) (empty ?p) (robot-location ?l))
    :effect (and (not (empty ?p)))
  )

  ; Serve plate p to a customer c in a table t in a location l that has to be a sevice area
  (:action serve-plate 
    :parameters (?p -plate ?l -service-area ?t -table ?c -customer)
    :precondition (and
      (not (served-plate ?p ?c)) 
      (not (served-customer ?c))
      (robot-location ?l)
      (table-location ?t ?l) 
      (customer-at ?c ?t)
      (carrying ?p) (not (empty ?p)) (wants ?c ?p) 
    )
    :effect (and
      (not (carrying ?p)) (robot-empty) 
      (plate-location ?p ?l) (served-plate ?p ?c)
    )
  )

  ; Move from location l1 to location l2
  (:action move
    :parameters (?l1 -location ?l2 -location)
    :precondition (and (robot-location ?l1) (adjacent ?l1 ?l2))
    :effect (and (not (robot-location ?l1)) (robot-location ?l2))
  )

  ; Mark customer as served
  (:action mark-customer-served
    :parameters (?c -customer)
    :precondition (and
      (forall (?p -plate)
        (or (not(wants ?c ?p)) (served-plate ?p ?c))
        ; equivalent to: (imply (wants ?c ?p) (served-plate ?p))
      )
    )
    :effect (and (served-customer ?c))
  )

  ; Mark table as served
  (:action mark-table-served
    :parameters (?t -table)
    :precondition (and
      (forall (?c -customer)
        (or (not(customer-at ?c ?t)) (served-customer ?c))
        ; equivalent to: (imply (customer-at ?c ?t) (served-customer ?c))
      )
    )
    :effect (and (served-table ?t))
  )
)