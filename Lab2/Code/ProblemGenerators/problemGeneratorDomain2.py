import random
import numpy as np
import time

def level(n):
    return "\t"*n

def endl():
    return "\n"

OUT_FILE = "../waiterRobot_Problem_Domain2_{time}.pddl".format(time=int(time.time()))

ALL_LOCATIONS = ['bta', 'aua', 'ama', 'ala', 'pla', 'pma', 'pua']
LOCATION_BUFFET = 'bta'
LOCATIONS_FOR_TABLES = ['aua', 'ama', 'ala', 'pla', 'pma', 'pua']

np.random.seed(int(time.time()))
N_CUSTOMERS = random.randint(1, 30)
N_PLATES    = random.randint(N_CUSTOMERS, N_CUSTOMERS*3)
N_TABLES    = random.randint(1, N_CUSTOMERS*2)

CUSTOMERS = ["c{n}".format(n=i+1) for i in range(N_CUSTOMERS)]
PLATES    = ["p{n}".format(n=i+1) for i in range(N_PLATES)]
TABLES    = ["t{n}".format(n=i+1) for i in range(N_TABLES)]

ROBOT_POS = [ALL_LOCATIONS[random.randint(0, 6)], ALL_LOCATIONS[random.randint(0, 6)]]
PLATES_POS = ['bta']*N_PLATES
TABLES_POS = [LOCATIONS_FOR_TABLES[random.randint(0, 5)] for _ in range(N_TABLES)]
CUSTOMERS_POS = [np.random.choice(TABLES) for _ in range(N_CUSTOMERS)]

output = ""
# --------------------------------------------------------------------------------------- 
output += level(0) + "(define (problem waiter-robot-problem)" + endl()
# ---------------------------------------------------------------------------------------
output += level(1) + "(:domain waiter-robot-domain2)" + endl()
# ---------------------------------------------------------------------------------------
output += level(1) + "(:objects" + endl()
output += level(2) + LOCATION_BUFFET + " -buffet-area" + endl()
output += level(2) + " ".join(LOCATIONS_FOR_TABLES) + " -service-area" + endl()
output += level(2) + " ".join(CUSTOMERS) + " -customer" + endl()
output += level(2) + " ".join(PLATES) + " -plate" + endl()
output += level(2) + " ".join(TABLES) + " -table" + endl()
output += level(1) + ")" + endl()
# ---------------------------------------------------------------------------------------
output += level(1) + "(:init" + endl()

for (location1, location2) in zip(ALL_LOCATIONS[:-1],ALL_LOCATIONS[1:]):
    output += level(2) + "(adjacent {location1} {location2})".format(location1=location1, location2=location2) + endl()
    output += level(2) + "(adjacent {location2} {location1})".format(location1=location1, location2=location2) + endl()

output += level(2) + "(adjacent {location2} {location1})".format(location1=ALL_LOCATIONS[1], location2=ALL_LOCATIONS[-1]) + endl()
output += level(2) + "(adjacent {location1} {location2})".format(location1=ALL_LOCATIONS[1], location2=ALL_LOCATIONS[-1]) + endl()

output += endl()

for idx, table in enumerate(TABLES):
    output += level(2) + "(table-location {table} {position})".format(table=table,position=TABLES_POS[idx]) + endl()

output += endl()

output += level(2) + "(robot-location {position})".format(position=ROBOT_POS[0]) + endl()
output += level(2) + "(robot-empty)" + endl()

output += endl()

for plate in PLATES:
    output += level(2) + "(plate-location {plate} bta)".format(plate=plate) + endl()
    output += level(2) + "(empty {plate})".format(plate=plate) + endl()

output += endl()

for idx, customer in enumerate(CUSTOMERS):
    output += level(2) + "(customer-at {customer} {table})".format(customer=customer,table=CUSTOMERS_POS[idx]) + endl()
    # At least one plate for customer
    output += level(2) + "(wants {customer} {plate})".format(customer=customer,plate=PLATES[idx]) + endl()
for plate in PLATES[N_CUSTOMERS:]:
    customer = np.random.choice(CUSTOMERS)
    output += level(2) + "(wants {customer} {plate})".format(customer=customer,plate=plate) + endl()

output += level(1) + ")" + endl()

# ---------------------------------------------------------------------------------------
output += level(1) + "(:goal" + endl()
output += level(2) + "(and" + endl()

output += level(3) + "(forall (?t -table)" + endl()
output += level(4) + "(served-table ?t)" + endl()
output += level(3) + ")" + endl()

output += level(3) + "(robot-location {position})".format(position=ROBOT_POS[1]) + endl()

output += level(2) + ")" + endl() 
output += level(1) + ")" + endl()
output += level(0) + ")"

# ---------------------------------------------------------------------------------------
text_file = open(OUT_FILE, "w")
text_file.write(output)
text_file.close()
