import random
import numpy as np
import time

def level(n):
    return "\t"*n

def endl():
    return "\n"

OUT_FILE = "../waiterRobot_Problem_Domain1_{time}.pddl".format(time=int(time.time()))

LOCATIONS = ['bta', 'aua', 'ama', 'ala', 'pla', 'pma', 'pua']
LOCATIONS_FOR_CUSTOMERS = ['aua', 'ama', 'ala', 'pla', 'pma', 'pua']

np.random.seed(int(time.time()))
N_PLATES    = random.randint(1, 15)
N_CUSTOMERS = N_PLATES

PLATES    = ["p{n}".format(n=i+1) for i in range(N_PLATES)]
CUSTOMERS = ["c{n}".format(n=i+1) for i in range(N_CUSTOMERS)]

ROBOT_POS = [LOCATIONS[random.randint(0, 6)], LOCATIONS[random.randint(0, 6)]]
PLATES_POS = ['bta']*N_PLATES
CUSTOMERS_POS = [LOCATIONS_FOR_CUSTOMERS[random.randint(0, 5)] for _ in range(N_CUSTOMERS)]

output = ""
# --------------------------------------------------------------------------------------- 
output += level(0) + "(define (problem waiter-robot-problem)" + endl()
# ---------------------------------------------------------------------------------------
output += level(1) + "(:domain waiter-robot-domain1)" + endl()
# ---------------------------------------------------------------------------------------
output += level(1) + "(:objects" + endl()
output += level(2) + "bta -buffet-area" + endl()
output += level(2) + " ".join(LOCATIONS_FOR_CUSTOMERS) + " -service-area" + endl()
output += level(2) + " ".join(CUSTOMERS) + " -customer" + endl()
output += level(2) + " ".join(PLATES) + " -plate" + endl()
output += level(1) + ")" + endl()
# ---------------------------------------------------------------------------------------
output += level(1) + "(:init" + endl()

for (location1, location2) in zip(LOCATIONS[:-1],LOCATIONS[1:]):
    output += level(2) + "(adjacent {location1} {location2})".format(location1=location1, location2=location2) + endl()
    output += level(2) + "(adjacent {location2} {location1})".format(location1=location1, location2=location2) + endl()

output += level(2) + "(adjacent {location2} {location1})".format(location1=LOCATIONS[1], location2=LOCATIONS[-1]) + endl()
output += level(2) + "(adjacent {location1} {location2})".format(location1=LOCATIONS[1], location2=LOCATIONS[-1]) + endl()

output += level(2) + "(robot-location {position})".format(position=ROBOT_POS[0]) + endl()
output += level(2) + "(robot-empty)" + endl()

for plate in PLATES:
    output += level(2) + "(plate-location {plate} bta)".format(plate=plate) + endl()
    output += level(2) + "(empty {plate})".format(plate=plate) + endl()

for idx, customer in enumerate(CUSTOMERS):
    output += level(2) + "(customer-at {customer} {location})".format(customer=customer,location=CUSTOMERS_POS[idx]) + endl()
    output += level(2) + "(wants {customer} {plate})".format(customer=customer,plate=PLATES[idx]) + endl()

output += level(1) + ")" + endl()

# ---------------------------------------------------------------------------------------
output += level(1) + "(:goal" + endl()
output += level(2) + "(and" + endl()

output += level(3) + "(forall (?c -customer)" + endl()
output += level(4) + "(served-customer ?c)" + endl()
output += level(3) + ")" + endl()

output += level(3) + "(robot-location {position})".format(position=ROBOT_POS[1]) + endl()

output += level(2) + ")" + endl() 
output += level(1) + ")" + endl()
output += level(0) + ")"

# ---------------------------------------------------------------------------------------
text_file = open(OUT_FILE, "w")
text_file.write(output)
text_file.close()
