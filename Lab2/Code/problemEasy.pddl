(define (problem waiter-robot-p1)
  (:domain waiter-robot-domain2)
  (:objects
    bta -buffet-area
    aua ama ala pua pma pla -service-area
    c1 -customer
    p1 -plate
    t1 -table
  )
  (:init
    ; Location connections
    (adjacent bta aua)
    (adjacent aua bta)
    (adjacent aua ama)
    (adjacent ama aua)
    (adjacent ama ala)
    (adjacent ala ama)
    (adjacent ala pla)
    (adjacent pla ala)
    (adjacent pla pma)
    (adjacent pma pla)
    (adjacent pma pua)
    (adjacent pua pma)
    (adjacent pua aua)
    (adjacent aua pua)

    ; Robot
    (robot-location bta)
    (robot-empty)

    ; Plates
    (plate-location p1 bta)
    (empty p1)

    ; Tables 
    (table-location t1 pma)

    ; Costumers
    (customer-at c1 t1)
    (wants c1 p1)
  )
  (:goal
    (and
      (forall (?t -table)
        (served-table ?t)
      )
      (robot-location bta)
    )
  )
)